package com.noimagination.srd.ring.powerup;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.noimagination.srd.ring.powerup.effect.TempEffect;
import com.noimagination.srd.ring.ship.Ship;

public class PowerUpTimer {
	public Label label;
	public final Ship ship;
	public final TempEffect effect;
	public float timer;
	public boolean ended;

	public PowerUpTimer (TempEffect effect, float time, Ship ship) {
		this.effect = effect;
		this.ship = ship;
		timer = time;
	}

	public void update (float delta) {
		effect.update(delta);
		timer -= delta;

		if (timer <= 0) {
			ended = true;
			effect.end(ship);
		}
	}
}
