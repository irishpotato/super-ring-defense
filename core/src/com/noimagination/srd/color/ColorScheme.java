package com.noimagination.srd.color;

import com.badlogic.gdx.graphics.Color;
import com.noimagination.srd.Assets;

public enum ColorScheme {
	DEFAULT (Assets.Colors.WHITE, Assets.Colors.BLACK),
	INVERT (Assets.Colors.BLACK, Assets.Colors.WHITE),
	TIMESTOP (Assets.Colors.BLUE, Assets.Colors.BLACK)
	;

	public final Color foreground;
	public final Color background;

	ColorScheme (Color foreground, Color background) {
		this.foreground = foreground;
		this.background = background;
	}
}
