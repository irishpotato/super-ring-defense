package com.noimagination.srd.ring.powerup.effect;

import com.badlogic.gdx.graphics.Color;
import com.noimagination.srd.ring.Usable;

public interface Effect extends Usable {
	Color getColor ();
	String getName ();
}
