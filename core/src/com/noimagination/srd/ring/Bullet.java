package com.noimagination.srd.ring;

import com.badlogic.gdx.graphics.Texture;
import com.noimagination.srd.Assets;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.particle.ShipFragmentParticle;
import com.noimagination.srd.ring.ship.Player;
import com.noimagination.srd.ring.ship.Ship;
import com.noimagination.srd.ring.ship.enemy.Enemy;
import com.noimagination.srd.utils.PolarVector;

import java.util.List;

public class Bullet extends RingObject {
	private final Ship shooter;
	private float speed;
	private int reflectTimes;

	public Bullet (Ship shooter) {
		super (new PolarVector(shooter.getAngle(), shooter.getDistance()));

		this.shooter = shooter;
		reflectTimes = 1;

		if (shooter instanceof Player) {
			setDistance(getDistance() - 40);
		} else {
			setDistance(getDistance() + 30);
		}

		setIgnoreColorScheme(true);
	}

	@Override
	public void moveForward (float speed) {
		if (shooter instanceof Player) {
			setDistance(getDistance() - speed);
		} else {
			setDistance(getDistance() + speed);
		}
	}

	@Override
	public void updateSpriteRotation () {
		getSprite().setRotation(getAngle() + 90);
	}

	private void updateColor() {
		if (shooter.reflect || shooter.superammo) {
			if (shooter.superammo) {
				getSprite().setColor(Assets.Colors.ORANGE);
			} else {	// У цвета superAmmo приоритет выше, чем у цвета bulletReflect
				getSprite().setColor(Assets.Colors.YELLOW);
			}
		} else {
			getSprite().setColor(shooter.getSprite().getColor());

		}
	}

	@Override
	public void update (float delta) {
		super.update(delta);
		if (!shooter.timemachine && Ring.SPEED <= 0) return;

		speed = shooter.getBulletSpeed() * (shooter.superammo ? 2.5f : 1);

		updateColor ();
		moveForward(speed);
		collisions();
	}

	private void reflect () {
		reflectTimes --;
		/*
		float cos = MathUtils.cos(getAngle() * MathUtils.degreesToRadians);
		float sin = MathUtils.sin(getAngle() * MathUtils.degreesToRadians);

		Vector2 moveVector = new Vector2(cos * speed, sin * speed);
		Vector2 hitNormal = new Vector2(getX() - Ring.CENTER.x, getY() - Ring.CENTER.y);
		Vector2 reflectionVector = new Vector2(
				moveVector.x - 2 * -(moveVector.x * hitNormal.x) * hitNormal.x,
				moveVector.y - 2 * -(moveVector.y * hitNormal.y) * hitNormal.y);
		*/
		setAngle(getAngle() - 20);
		//todo setAngle (360 - getAngle());*/
		/*
		float reflection = (SuperRingDefense.getRandom().nextInt(2) - 1) * 45;
		setAngle(getAngle() + reflection);
		setDistance(getDistance() - 50);*/
		//setAngle(Utils.getAngleBetween(getCartesianPosition(), reflectionVector));
		//setAngle(360 - getAngle());
		//getPosition().add(180, -10);
		//getPosition().add(-cos * speed * 2, -sin * speed * 2);
	}

	private void collisions () {
		// Math.abs - костыль
		if (Math.abs(getDistance()) > Ring.RADIUS - 20) {
			if (shooter.reflect && reflectTimes > 0) {
				reflect();
			} else {
				die();
			}
		}

		List<Bullet> bulletList = SuperRingDefense.getGame().ring.bullets;
		for (int i = 0; i < bulletList.size(); i++) {
			Bullet bullet = bulletList.get(i);

			if (bullet.shooter != shooter && bullet.getAABB().overlaps(getAABB())) {
				bullet.die();
				die();
				break;
			}
		}

		if (shooter instanceof Player) {
			List<Enemy> enemyList = SuperRingDefense.getGame().ring.enemies;
			for (int i = 0; i < enemyList.size(); i++) {
				Enemy enemy = enemyList.get(i);
				if (!enemy.invincible && enemy.getAABB().overlaps(getAABB())) {
					enemy.hit(shooter.getDamage());
					enemy.setLastHitShip(shooter);
					die();
					if (!enemy.isDead()) ShipFragmentParticle.emit(enemy.getEmitSize(), enemy, getAngle());
					break;
				}
			}
		} else if (shooter instanceof Enemy) {
			if (SuperRingDefense.getGame().getPlayer().getAABB().overlaps(getAABB())) {
				SuperRingDefense.getGame().getPlayer().hit(shooter.getDamage());
				ShipFragmentParticle.emit(SuperRingDefense.getGame().getPlayer().getEmitSize(), SuperRingDefense.getGame().getPlayer(), getAngle());
				die();
			}
		}

	}


	@Override
	public Texture getTexture() {
		return Assets.Textures.BULLET;
	}
}
