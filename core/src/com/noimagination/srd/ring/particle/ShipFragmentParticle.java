package com.noimagination.srd.ring.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.noimagination.srd.Assets;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.ship.Player;
import com.noimagination.srd.ring.ship.Ship;
import com.noimagination.srd.ring.ship.enemy.Enemy;

public class ShipFragmentParticle extends Particle {

	private final Ship ship;

	private ShipFragmentParticle(Ship ship, float angle) {
		super(ship.getPosition());
		this.ship = ship;
		updateColor();
	}

	private ShipFragmentParticle(Ship ship) {
		this (ship, SuperRingDefense.getRandom().nextInt(360));
	}

	public static void emit(EmitSize size, Ship ship) {
		for (int i = 0; i < size.ordinal() * 3 + SuperRingDefense.getRandom().nextInt(size.ordinal() * 3 / 2) + 1; i++) {
			SuperRingDefense.getGame().ring.particles.add(new ShipFragmentParticle(ship));
		}
	}

	public static void emit(EmitSize size, Ship ship, float angle) {
		for (int i = 0; i < size.ordinal() * 3 + SuperRingDefense.getRandom().nextInt(size.ordinal() * 3 / 2) + 1; i++) {
			// fixme помоему быдлокод
			if (ship instanceof Enemy) {
				Enemy enemy = (Enemy) ship;
				SuperRingDefense.getGame().ring.particles.add(new ShipFragmentParticle(enemy));

				/*
				if (enemy.lastHitBullet != null) {
					SuperRingDefense.getGame().ring.particles.add(new ShipFragmentParticle(enemy, angle));
				} else {
					SuperRingDefense.getGame().ring.particles.add(new ShipFragmentParticle(ship));
				}*/
			} else if (ship instanceof Player) {
				Player player = (Player) ship;
				SuperRingDefense.getGame().ring.particles.add(new ShipFragmentParticle(player, angle));
			}
		}
	}

	@Override
	public Color getColor() {
		return ship.getSprite().getColor();
	}

	@Override
	public Texture[] getTextureSet() {
		return new Texture[] {
				Assets.Textures.FRAGMENT_1,
				Assets.Textures.FRAGMENT_2
		};
	}
}
