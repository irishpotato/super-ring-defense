package com.noimagination.srd.ring.powerup.effect;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.noimagination.srd.Assets;
import com.noimagination.srd.ring.ship.Ship;

public class ReflectEffect implements TempEffect {

	@Override
	public void use(Ship ship) {
		ship.reflect = true;
	}

	@Override
	public void end(Ship ship) {
		ship.reflect = false;
	}

	@Override
	public float getDefaultTime() {
		return 5f;
	}

	@Override
	public void update(float delta) {

	}

	@Override
	public Sound getSound() {
		return Assets.Sounds.POWERUP_PICK;
	}

	@Override
	public Color getColor() {
		return Assets.Colors.YELLOW;
	}

	@Override
	public String getName() {
		return "Reflect";
	}
}
