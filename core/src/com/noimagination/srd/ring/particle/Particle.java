package com.noimagination.srd.ring.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.Ring;
import com.noimagination.srd.ring.RingObject;
import com.noimagination.srd.utils.PolarVector;

public abstract class Particle extends RingObject {

	Particle(PolarVector position) {
		super(
				new PolarVector(
						position.getAngle() + SuperRingDefense.getRandom().nextInt(13) - 6,
						position.getDistance() + SuperRingDefense.getRandom().nextInt(81) - 40
				)
		);

		// todo реализовать плавный поворот частицы в рандомную сторону
		getSprite().setRotation(position.getAngle() - 25f + SuperRingDefense.getRandom().nextInt(50));
		setIgnoreColorScheme(true);
	}

	public abstract Color getColor ();

	public abstract Texture[] getTextureSet ();

	protected void updateColor () {
		getSprite().setColor(getColor().cpy());
	}

	@Override
	public Texture getTexture () {
		final Texture[] textureSet = getTextureSet();
		return textureSet[SuperRingDefense.getRandom().nextInt(textureSet.length)];
	}

	@Override
	protected void updateSprite () {
		Vector2 cartesian = PolarVector.polarToCartesian(getPosition(), Ring.CENTER);
		getSprite().setCenter(cartesian.x, cartesian.y);
	}

	public enum EmitSize {
		SMALL, NORMAL, BIG, HUGE
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		moveForward(Ring.SPEED);
		fade(delta);
	}

	private void fade (float delta) {
		if (timer > 0.9f / Ring.SPEED) {
			float alpha = getSprite().getColor().a - 0.025f * Ring.SPEED;
			getSprite().setAlpha(alpha);

			if (alpha <= 0) {
				die();
			}
		}
	}
}
