package com.noimagination.srd;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.noimagination.srd.screens.GameScreen;

import java.util.Random;

//fixme баг: коллайдер стены (когда стена уже вышла за кольцо, коллайдер все еще работает)
//fixme баг: когда реплей начинается позже отсчета до окончания паузы (как бы пофикшено)
//todo: сделать так, что бы при начислении некоторого колва очков label со score загорался цветом
//todo: плавный счетчик очков
//todo: сделать редизайн абстракции врагов (напр. abstract update который вызывается из обычного update), а затем создать босса

public class SuperRingDefense extends Game {
	public static final int WIDTH = 900;
	public static final int HEIGHT = 1600;
	private static Random random;
	private static GameScreen game;
	private static Preferences prefs;

	@Override
	public void create () {
		prefs = Gdx.app.getPreferences("Preferences");
		random = new Random();
		Assets.init();
		game = new GameScreen();
		setScreen(game);
	}

	public static Preferences getPrefs () {
		return prefs;
	}

	public static GameScreen getGame() {
		return game;
	}

	public static Random getRandom () {
		return random;
	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void dispose () {
		super.dispose();
	}
}
