package com.noimagination.srd.ring;

import com.noimagination.srd.color.ColorScheme;
import com.noimagination.srd.ring.ship.enemy.EnemyType;

public enum Level {
	FIRST (ColorScheme.DEFAULT,
			new EnemyType[] {
					EnemyType.ASSAULT
			}, new Map.MapType[] {
					Map.MapType.ENEMY_WAVE
			}),

	SECOND (ColorScheme.DEFAULT,
			new EnemyType[] {
					EnemyType.ASSAULT,
					EnemyType.HARD
			}, new Map.MapType[] {
					Map.MapType.ENEMY_WAVE,
			});

	private final ColorScheme colorScheme;
	private final EnemyType[] enemyTypes;
	private final Map.MapType[] mapTypes;

	Level (ColorScheme colorScheme, EnemyType[] enemyTypes, Map.MapType[] mapTypes) {
		this.colorScheme = colorScheme;
		this.enemyTypes = enemyTypes;
		this.mapTypes = mapTypes;
	}

	public ColorScheme getColorScheme () {
		return colorScheme;
	}

	public EnemyType[] getEnemyTypes () {
		return enemyTypes;
	}

	public Map.MapType[] getMapTypes () {
		return mapTypes;
	}
}
