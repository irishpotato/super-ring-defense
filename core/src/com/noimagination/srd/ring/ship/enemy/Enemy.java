package com.noimagination.srd.ring.ship.enemy;

import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.Ring;
import com.noimagination.srd.ring.particle.ShipFragmentParticle;
import com.noimagination.srd.ring.ship.Ship;
import com.noimagination.srd.utils.PolarVector;

public abstract class Enemy extends Ship {

	private Ship lastHitShip;

	protected Enemy(int section) {
		super (new PolarVector(Ring.normalizeSection(section) * 45 + 22.5f, 0));
		getSprite().setAlpha(0f);
	}

	abstract public int getReward();

	@Override
	public void update (float delta) {
		super.update(delta);
		moveForward(Ring.SPEED);
		collisions();
	}

	@Override
	public void die () {
		super.die();
		if (lastHitShip != null) {
			SuperRingDefense.getGame().getScoreManager().addScore(getReward());
		}

		ShipFragmentParticle.emit(getEmitSize(), this);
	}

	public void setLastHitShip (Ship lastHitShip) {
		this.lastHitShip = lastHitShip;
	}

	private void collisions () {
		if (getDistance() >= Ring.RADIUS - 37) {
			die();
			SuperRingDefense.getGame().getPlayer().hit(1);
		}

		if (getAABB().overlaps(SuperRingDefense.getGame().getPlayer().getAABB())) {
			SuperRingDefense.getGame().getPlayer().hit(1);
		}
	}
}
