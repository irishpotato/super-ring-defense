package com.noimagination.srd.ring.powerup.effect;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.noimagination.srd.Assets;
import com.noimagination.srd.ring.ship.Ship;

public class RestoreEffect implements Effect {

	@Override
	public void use(Ship ship) {
		ship.restore();
	}

	@Override
	public Sound getSound() {
		return Assets.Sounds.POWERUP_PICK;
	}

	@Override
	public Color getColor() {
		return Assets.Colors.RED;
	}

	@Override
	public String getName() {
		return "Health";
	}
}
