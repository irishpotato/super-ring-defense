package com.noimagination.srd.ring.ship.enemy;

import com.noimagination.srd.SuperRingDefense;

// Пример использования:
// EnemyType[] allowedEnemies = new EnemyType[] {EnemyType.ASSAULT, EnemyType.HARD}
public enum EnemyType {
	ASSAULT {
		@Override
		public Enemy getInstance(int section) {
			return new AssaultEnemy(section);
		}
	},

	HARD {
		@Override
		public Enemy getInstance(int section) {
			return new HardEnemy(section);
		}
	},
	;

	public static Enemy getRandom (int section) {
		return values()[SuperRingDefense.getRandom().nextInt(values().length)].getInstance(section);
	}

	public abstract Enemy getInstance(int section);

	/*
	private final float chance;
	EnemyType (float chance) {
		this.chance = chance;
	}*/
}
