package com.noimagination.srd.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.noimagination.srd.Assets;
import com.noimagination.srd.CustomCamera;
import com.noimagination.srd.CustomShapeRenderer;
import com.noimagination.srd.ScoreManager;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.color.ColorScheme;
import com.noimagination.srd.ring.Level;
import com.noimagination.srd.ring.Ring;
import com.noimagination.srd.ring.ship.Player;
import com.noimagination.srd.ui.GameUI;

public class GameScreen implements Screen {

	/*
	 *	Ring ring,
	  *	SpriteBatch batch,
	  *	CustomShapeRenderer shapeRenderer,
	  *	GameUI ui,
	 */
	public final Ring ring;
	private final SpriteBatch batch;
	private final CustomShapeRenderer shapeRenderer;
	private float scoreTimer;
	private float respawnTimer;
	public float pauseTimer;
	private float pauseSecondTimer;
	private boolean paused;
	public final GameUI ui;
	private final Viewport viewport;
	private final CustomCamera camera;
	private final Level level;
	private final ScoreManager scoreManager;
	private ColorScheme colorScheme;

	public GameScreen () {
		scoreManager = new ScoreManager();

		level = Level.FIRST;
		setColorScheme(level.getColorScheme());

		Gdx.gl.glLineWidth(4);
		camera = new CustomCamera(new OrthographicCamera(SuperRingDefense.WIDTH, SuperRingDefense.HEIGHT));
		viewport = new FitViewport(SuperRingDefense.WIDTH, SuperRingDefense.HEIGHT, camera.getOrthographicCamera());
		ring = new Ring();
		batch = new SpriteBatch();
		shapeRenderer = new CustomShapeRenderer();
		ui = new GameUI(viewport);
		Gdx.input.setInputProcessor(ui.getStage());
	}

	public boolean isPaused () {
		return paused;
	}

	public ScoreManager getScoreManager () {
		return scoreManager;
	}

	public Player getPlayer () {
		return ring.getPlayer();
	}

	public Level getLevel () {
		return level;
	}

	public ColorScheme getColorScheme () {
		return colorScheme;
	}

	public void setColorScheme (ColorScheme colorScheme) {
		this.colorScheme = colorScheme;
	}

	@Override
	public void show() {

	}

	@Override
	public void render(float delta) {
		update(delta);
		draw();
	}

	private void updatePauseTimer (float delta) {
		if (!paused || pauseTimer <= 0) return;
		pauseTimer -= delta;
		pauseSecondTimer -= delta;

		if (pauseSecondTimer < 0) {
			switch ((int)pauseTimer + 1) {
				case 3:
					Assets.Sounds.TICK_1.play();
					break;
				case 2:
					Assets.Sounds.TICK_2.play();
					break;
				case 1:
					Assets.Sounds.TICK_3.play();
					break;
			}

			ui.timerLabel.addAction(
					Actions.sequence(
							Actions.alpha(1f),
							Actions.delay(0.3f),
							Actions.fadeOut(0.5f),
							Actions.delay(0.2f)
					)
			);

			pauseSecondTimer = 1f;
		}

		if (pauseTimer < 0) {
			resume();
		}
	}

	private void updateScoreTimer(float delta) {
		if (ring.isStopped() || Ring.SPEED == 0) return;
		scoreTimer += delta;

		if (scoreTimer >= 2f / Ring.SPEED) {
			scoreTimer = 0;
			scoreManager.addScore(1);
		}
	}

	private void updateRespawnTimer (float delta) {
		if (SuperRingDefense.getGame().getPlayer().isDead()) {
			respawnTimer += delta;

			if (respawnTimer >= 2f) {
				respawnTimer = 0;
				restart();
			}
		}
	}

	public void restart () {
		ring.setPlayer(new Player());	// fixme new Player () - хуйня
		scoreManager.reset();
		scoreTimer = 0;
		pauseTimer = 0;
		paused = false;
		ring.reset();
		ui.fadePowerUpLabels();
	}

	private void update(float delta) {
		updateRespawnTimer(delta);
		updateScoreTimer(delta);
		updatePauseTimer(delta);
		scoreManager.update();
		ui.update(delta);
		ring.update(delta);
		camera.update(delta);
	}

	private void draw() {
		Gdx.gl.glClearColor(getColorScheme().background.r, getColorScheme().background.g, getColorScheme().background.b, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//oldGameLook();
		ring.draw (batch, shapeRenderer);
		ui.draw();
	}

	public CustomCamera getCamera () {
		return camera;
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}

	@Override
	public void pause() {
		if (paused) return;
		paused = true;
		ring.stop();
	}

	@Override
	public void resume() {
		if (!paused) return;
		paused = false;
		ring.resume();
	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		// todo dispose
		batch.dispose();
		Assets.Textures.RING.dispose();
	}
}
