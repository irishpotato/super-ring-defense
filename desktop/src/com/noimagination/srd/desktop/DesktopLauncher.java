package com.noimagination.srd.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.noimagination.srd.SuperRingDefense;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new SuperRingDefense(), config);
		config.samples = 4;
		config.resizable = true;
		config.width = SuperRingDefense.WIDTH / 2;
		config.height = SuperRingDefense.HEIGHT / 2;
	}
}
