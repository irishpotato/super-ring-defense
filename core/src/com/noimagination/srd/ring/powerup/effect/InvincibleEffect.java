package com.noimagination.srd.ring.powerup.effect;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.noimagination.srd.Assets;
import com.noimagination.srd.ring.ship.Ship;

public class InvincibleEffect implements TempEffect {

	@Override
	public float getDefaultTime() {
		return 3;
	}

	@Override
	public void update(float delta) {

	}

	@Override
	public void end(Ship ship) {
		ship.invincible = false;
	}

	@Override
	public void use(Ship ship) {
		ship.invincible = true;
	}

	@Override
	public Sound getSound() {
		return Assets.Sounds.POWERUP_PICK;
	}

	@Override
	public Color getColor() {
		return Assets.Colors.RED;
	}

	@Override
	public String getName() {
		return "Invincible";
	}
}
