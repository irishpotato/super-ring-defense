package com.noimagination.srd.ring.powerup.effect;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.noimagination.srd.Assets;
import com.noimagination.srd.ring.ship.Ship;

public class SuperAmmoEffect implements TempEffect {

	@Override
	public void use(Ship ship) {
		ship.superammo = true;
	}

	@Override
	public void end(Ship ship) {
		ship.superammo = false;
	}

	@Override
	public float getDefaultTime() {
		return 5;
	}

	@Override
	public void update(float delta) {

	}

	@Override
	public Sound getSound() {
		return Assets.Sounds.POWERUP_PICK;
	}

	@Override
	public Color getColor() {
		return Assets.Colors.ORANGE;
	}

	@Override
	public String getName() {
		return "Super Ammo";
	}
}
