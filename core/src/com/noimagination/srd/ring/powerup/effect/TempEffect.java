package com.noimagination.srd.ring.powerup.effect;

import com.noimagination.srd.ring.ship.Ship;

public interface TempEffect extends Effect {
	void update(float delta);
	void end(Ship ship);
	float getDefaultTime();
}
