package com.noimagination.srd;

public class ScoreManager {

	private int score;
	private int highscore;
	private boolean highscoreBeaten;

	public ScoreManager () {
		highscore = SuperRingDefense.getPrefs().getInteger("highscore", 0);
	}

	public void update () {
		if (getScore() > getHighscore()) {
			updateHighscore();
		}
	}

	public void updateHighscore() {
		highscore = score;
		SuperRingDefense.getPrefs().putInteger("highscore", highscore);
		SuperRingDefense.getPrefs().flush();

		if (!highscoreBeaten) {
			highscoreBeaten = true;
			SuperRingDefense.getGame().ui.newHighscore();
		}
	}

	public void reset () {
		highscoreBeaten = false;
		score = 0;
	}

	public int getHighscore () {
		return highscore;
	}

	public void addScore (int score) {
		this.score += score;
	}

	public int getScore () {
		return score;
	}
}
