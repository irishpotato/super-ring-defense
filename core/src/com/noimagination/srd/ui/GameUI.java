package com.noimagination.srd.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.noimagination.srd.Assets;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.Level;
import com.noimagination.srd.ring.powerup.effect.TempEffect;
import com.noimagination.srd.ring.ship.Player;

public class GameUI {

	private Scene scene;
	private final Stage stage;
	private final Label scoreLabel;
	private final Label livesLabel;
	private final Label pauseLabel;
	public final Label timerLabel;
	private final Label gameOverLabel;
	private final Label tapToStartLabel;
	private final Label levelNameLabel;
	private final Image title;
	private final Button pauseButton;
	private final Button replayButton;
	private final Button exitButton;
	private final Button playButton;
	private final Image pausePlaneImage;
	private final VerticalGroup POWERUPS;
	private final Group GAME_PAUSE;
	private final Group GAME;
	private final Group START;
	private final Group LEVELS;
	private final Label.LabelStyle defaultLabelStyle = new Label.LabelStyle(Assets.Fonts.DEFAULT, Assets.Colors.WHITE);
	private final Image botBlackLine;
	private final Image topBlackLine;
	private final Image[] hearts = new Image[3];
	private final Group HEARTS;
	private Level selectedLevel;

	public void fadeHeartsRed() {
		for (Image image : hearts) {
			image.addAction(Actions.sequence(
					Actions.color(Assets.Colors.RED, 0.15f),
					Actions.color(SuperRingDefense.getGame().getColorScheme().foreground)
			));
		}
	}

	public enum Scene {
		START, LEVELS, GAME
	}

	public GameUI(Viewport viewport) {
		stage = new Stage(viewport);

		// Спасибо LibGDX
		TextureRegionDrawable pauseButtonDrawable = new TextureRegionDrawable(new TextureRegion(Assets.Textures.PAUSE_BUTTON));
		TextureRegionDrawable replayButtonDrawable = new TextureRegionDrawable(new TextureRegion(Assets.Textures.REPLAY_BUTTON));
		TextureRegionDrawable exitButtonDrawable = new TextureRegionDrawable(new TextureRegion(Assets.Textures.EXIT_BUTTON));
		TextureRegionDrawable playButtonDrawable = new TextureRegionDrawable(new TextureRegion(Assets.Textures.PLAY_BUTTON));
		TextureRegionDrawable pauseButtonPressedDrawable = new TextureRegionDrawable(new TextureRegion(Assets.Textures.PAUSE_BUTTON_PRESSED));
		TextureRegionDrawable replayButtonPressedDrawable = new TextureRegionDrawable(new TextureRegion(Assets.Textures.REPLAY_BUTTON_PRESSED));
		TextureRegionDrawable exitButtonPressedDrawable = new TextureRegionDrawable(new TextureRegion(Assets.Textures.EXIT_BUTTON_PRESSED));
		TextureRegionDrawable playButtonPressedDrawable = new TextureRegionDrawable(new TextureRegion(Assets.Textures.PLAY_BUTTON_PRESSED));

		Button.ButtonStyle pauseButtonStyle = new Button.ButtonStyle(pauseButtonDrawable, pauseButtonPressedDrawable, pauseButtonDrawable);
		Button.ButtonStyle replayButtonStyle = new Button.ButtonStyle(replayButtonDrawable, replayButtonPressedDrawable, replayButtonDrawable);
		Button.ButtonStyle exitButtonStyle = new Button.ButtonStyle(exitButtonDrawable, exitButtonPressedDrawable, exitButtonDrawable);
		Button.ButtonStyle playButtonStyle = new Button.ButtonStyle(playButtonDrawable, playButtonPressedDrawable, playButtonDrawable);

		GAME = new Group();
		{
			// Pause Button
			pauseButton = new Button(pauseButtonStyle);
			pauseButton.addListener(new ClickListener() {
				@Override
				public void clicked (InputEvent event, float x, float y)  {
					if (SuperRingDefense.getGame().getPlayer().isDead()) return;

					if (SuperRingDefense.getGame().pauseTimer <= 0) {
						if (!SuperRingDefense.getGame().isPaused()) {
							SuperRingDefense.getGame().pause();
							Assets.Sounds.CLICK_1.play();
						}
					}
				}
			});

			pauseButton.setPosition(
					SuperRingDefense.WIDTH - Assets.Textures.PAUSE_BUTTON.getWidth() - 20,
					SuperRingDefense.HEIGHT - Assets.Textures.PAUSE_BUTTON.getHeight() - 20);
			GAME.addActor(pauseButton);

			/** Game Pause Group **/
			GAME_PAUSE = new Group();
			{
				// Plane Image
				pausePlaneImage = new Image(Assets.Textures.PAUSE_BLACK_PLANE);
				pausePlaneImage.setPosition(SuperRingDefense.WIDTH / 2, SuperRingDefense.HEIGHT / 2, Align.center);
				GAME_PAUSE.addActor(pausePlaneImage);

				// Exit Button
				exitButton = new Button(exitButtonStyle);
				exitButton.addListener(new ClickListener() {
					@Override
					public void clicked (InputEvent event, float x, float y)  {
						start();
						Assets.Sounds.CLICK_2.play();
					}
				});
				exitButton.setPosition(
						SuperRingDefense.WIDTH / 8 * 3 - 15,
						SuperRingDefense.HEIGHT / 2 - 40, Align.center
				);
				GAME_PAUSE.addActor(exitButton);

				// Replay Button
				replayButton = new Button(replayButtonStyle);
				replayButton.addListener(new ClickListener() {
					@Override
					public void clicked (InputEvent event, float x, float y)  {
						SuperRingDefense.getGame().restart();
						Assets.Sounds.CLICK_2.play();
					}
				});
				replayButton.setPosition(
						SuperRingDefense.WIDTH / 8 * 5 + 15,
						SuperRingDefense.HEIGHT / 2 - 40, Align.center);
				GAME_PAUSE.addActor(replayButton);

				// Play Button
				playButton = new Button(playButtonStyle);
				playButton.addListener(new ClickListener() {
					@Override
					public void clicked (InputEvent event, float x, float y)  {
						SuperRingDefense.getGame().pauseTimer = 2.99f;
						//Assets.Sounds.CLICK_2.play();
					}
				});
				playButton.setPosition(
						SuperRingDefense.WIDTH / 8 * 4,
						SuperRingDefense.HEIGHT / 2 - 40, Align.center
				);
				GAME_PAUSE.addActor(playButton);

				// Pause Label
				pauseLabel = new Label("PAUSED", defaultLabelStyle);
				pauseLabel.setPosition(SuperRingDefense.WIDTH / 2, SuperRingDefense.HEIGHT / 2 + 80, Align.center);
				GAME_PAUSE.addActor(pauseLabel);

				// Timer Label
				//fixme таймер не показывается
				timerLabel = new Label("3", defaultLabelStyle);
				timerLabel.setColor(Assets.Colors.RED);
				timerLabel.setPosition(SuperRingDefense.WIDTH / 2, SuperRingDefense.HEIGHT / 2, Align.center);
				GAME_PAUSE.addActor(timerLabel);
			}
			GAME.addActor(GAME_PAUSE);

			// Score Label
			scoreLabel = new Label("SCORE\n" + "00000", defaultLabelStyle);
			scoreLabel.setPosition(SuperRingDefense.WIDTH / 5, 1300, Align.top);
			GAME.addActor(scoreLabel);

			// Lives Label
			livesLabel = new Label("LIVES", defaultLabelStyle);
			livesLabel.setPosition(SuperRingDefense.WIDTH / 5 * 4, 1300, Align.top);

			HEARTS = new Group();
			HEARTS.setPosition(SuperRingDefense.WIDTH / 5 * 4, 1270 - 30, Align.center);

			for (int i = 0; i < 3; i++) {
				hearts[i] = new Image(Assets.Textures.FULL_HEART);
				hearts[i].setPosition(i * 50 - 50, 0, Align.center);
				HEARTS.addActor(hearts[i]);
			}


			GAME.addActor(HEARTS);
			GAME.addActor(livesLabel);

			// Game Over Label
			gameOverLabel = new Label("GAME OVER", defaultLabelStyle);
			gameOverLabel.setPosition(SuperRingDefense.WIDTH / 2, SuperRingDefense.HEIGHT / 2, Align.center);
			gameOverLabel.setColor(Assets.Colors.RED);
			GAME.addActor(gameOverLabel);

			/* PowerUps Group */
			POWERUPS = new VerticalGroup();
			POWERUPS.setPosition(0, 1360, Align.center);
			POWERUPS.setSize(SuperRingDefense.WIDTH, SuperRingDefense.HEIGHT - 1360);
			POWERUPS.space(20);
			GAME.addActor(POWERUPS);
		}
		stage.addActor(GAME);

		botBlackLine = new Image(Assets.Textures.BLACK_LINE);
		topBlackLine = new Image(Assets.Textures.BLACK_LINE);
		botBlackLine.setPosition(0, -Assets.Textures.BLACK_LINE.getHeight());
		topBlackLine.setPosition(0, SuperRingDefense.HEIGHT);
		botBlackLine.setTouchable(Touchable.disabled);
		topBlackLine.setTouchable(Touchable.disabled);
		stage.addActor(botBlackLine);
		stage.addActor(topBlackLine);

		tapToStartLabel = new Label("TAP TO START", defaultLabelStyle);
		tapToStartLabel.setPosition(SuperRingDefense.WIDTH / 2, botBlackLine.getHeight() / 2, Align.center);
		tapToStartLabel.addAction(Actions.forever(Actions.sequence(Actions.fadeOut(0.8f), Actions.fadeIn(0.8f))));
		stage.addActor(tapToStartLabel);

		START = new Group();
		{
			Actor tapAnywhere = new Actor();
			tapAnywhere.setSize(SuperRingDefense.WIDTH, SuperRingDefense.HEIGHT - Assets.Textures.BLACK_LINE.getHeight());
			tapAnywhere.addListener(new ClickListener() {
				@Override
				public void clicked (InputEvent event, float x, float y)  {
					levels();
					Assets.Sounds.CLICK_2.play();
				}
			});
			START.addActor(tapAnywhere);

			title = new Image(Assets.Textures.TITLE);
			title.setPosition(SuperRingDefense.WIDTH / 2, SuperRingDefense.HEIGHT / 2, Align.center);
			title.setTouchable(Touchable.disabled);
			START.addActor(title);
		}
		stage.addActor(START);

		LEVELS = new Group();
		{
			Actor tapAnywhere = new Actor();
			tapAnywhere.setSize(SuperRingDefense.WIDTH, SuperRingDefense.HEIGHT - Assets.Textures.BLACK_LINE.getHeight());
			tapAnywhere.addListener(new ClickListener() {
				@Override
				public void clicked (InputEvent event, float x, float y)  {
					game();
					Assets.Sounds.CLICK_2.play();
				}
			});
			LEVELS.addActor(tapAnywhere);

			levelNameLabel = new Label("LEVEL\n  1", defaultLabelStyle);
			levelNameLabel.setPosition(SuperRingDefense.WIDTH / 2, SuperRingDefense.HEIGHT / 2, Align.center);
			levelNameLabel.setTouchable(Touchable.disabled);
			LEVELS.addActor(levelNameLabel);
		}
		stage.addActor(LEVELS);

		//levels();
		start();
		//game();
	}

	public Stage getStage () {
		return stage;
	}

	public void start () {
		scene = Scene.START;
		showBlackLines();
	}

	public void levels () {
		scene = Scene.LEVELS;
		showBlackLines();
	}

	public void game() {
		scene = Scene.GAME;
		hideBlackLines();
		SuperRingDefense.getGame().restart();
	}

	private void updateHearts () {
		Player player = SuperRingDefense.getGame().getPlayer();
		for (int i = 0; i < hearts.length; i++) {
			hearts[i].setVisible(i + 1 <= player.getHealth());
		}
	}

	private void showBlackLines() {
		botBlackLine.addAction(Actions.moveTo(0, 0, 0.4f, Interpolation.exp10Out));
		topBlackLine.addAction(Actions.moveTo(0, SuperRingDefense.HEIGHT - Assets.Textures.BLACK_LINE.getHeight(), 0.4f, Interpolation.exp10Out));
	}

	private void hideBlackLines() {
		botBlackLine.addAction(Actions.moveTo(0, -Assets.Textures.BLACK_LINE.getHeight(), 0.4f, Interpolation.exp10Out));
		topBlackLine.addAction(Actions.moveTo(0, SuperRingDefense.HEIGHT, 0.4f, Interpolation.exp10Out));
	}

	public Label addPowerUpLabel(TempEffect effect, float time) {
		//if (!(effect instanceof TempEffect)) return null;
		Label label = new Label(effect.getName().toUpperCase(), defaultLabelStyle);
		label.setColor(effect.getColor());
		label.addAction(Actions.sequence(Actions.delay(time), Actions.fadeOut(0.5f)));
		POWERUPS.center().bottom().addActor(label);
		return label;
	}

	public void newHighscore () {
		scoreLabel.addAction(
				Actions.repeat(3,
						Actions.sequence(
								Actions.color(Assets.Colors.RED),
								Actions.delay(0.4f),
								Actions.color(Assets.Colors.WHITE),
								Actions.delay(0.5f)
						)
				)
		);
	}

	public void fadePowerUpLabels () {
		for (Actor actor : POWERUPS.getChildren()) {
			actor.addAction(Actions.fadeOut(0.5f));
		}
	}

	public void update(float delta) {
		Color foregroundColor = SuperRingDefense.getGame().getColorScheme().foreground;
		scoreLabel.setColor(foregroundColor);
		scoreLabel.setText("SCORE\n" + String.format("%05d", SuperRingDefense.getGame().getScoreManager().getScore()));
		//livesLabel.setText("LIVES\n" + " " + String.format("%03d", SuperRingDefense.getGame().getPlayer().health));
		livesLabel.setColor(foregroundColor);
		livesLabel.setText("LIVES");
		for (Image image : hearts) {
			image.setColor(foregroundColor);
		}

		START.setVisible(scene == Scene.START);
		LEVELS.setVisible(scene == Scene.LEVELS);
		GAME.setVisible(scene == Scene.GAME);
		tapToStartLabel.setVisible(scene == Scene.START || scene == Scene.LEVELS);

		if (scene == Scene.GAME) {
			timerLabel.setVisible(SuperRingDefense.getGame().isPaused() && SuperRingDefense.getGame().pauseTimer > 0);
			pauseButton.setColor(foregroundColor);
			GAME_PAUSE.setVisible(SuperRingDefense.getGame().isPaused() && SuperRingDefense.getGame().pauseTimer <= 0);
			gameOverLabel.setVisible(SuperRingDefense.getGame().getPlayer().isDead());
			updateHearts();

			if (SuperRingDefense.getGame().isPaused()) {
				if (SuperRingDefense.getGame().pauseTimer <= 0) {
					pauseLabel.setText("PAUSED");
				} else {
					timerLabel.setText("" + ((int) SuperRingDefense.getGame().pauseTimer + 1));
				}
			}
		}

		// Конструкция ниже обновляет весь stage за исключением POWERUPS во время паузы
		for (Actor actor : stage.getActors()) {
			//fixme помоему быдлокод
			if (actor == POWERUPS) {
				if (!SuperRingDefense.getGame().isPaused())
				{
					actor.act(delta);
				}
			} else {
				actor.act (delta);
			}
		}
	}

	public void draw() {
		stage.draw();
	}
}
