package com.noimagination.srd.ring.powerup.effect;

import com.noimagination.srd.SuperRingDefense;

public enum EffectType {
	REFLECT {
		@Override
		public Effect getInstance() {
			return new ReflectEffect();
		}
	},

	RESTORE {
		@Override
		public Effect getInstance() {
			return new RestoreEffect();
		}
	},

	INVINCIBLE {
		@Override
		public Effect getInstance() {
			return new InvincibleEffect();
		}
	},

	SUPERAMMO {
		@Override
		public Effect getInstance() {
			return new SuperAmmoEffect();
		}
	},

	TIMEMACHINE {
		@Override
		public Effect getInstance() {
			return new TimeMachineEffect();
		}
	},
	;

	public static EffectType getRandom () {
		return values()[SuperRingDefense.getRandom().nextInt(values().length)];
	}

	public abstract Effect getInstance();
}