package com.noimagination.srd.utils;

/**
 * Костыль (виноват не я если чо)
 */
public class Pair<F, S> {
	public final F first;
	public final S second;

	public Pair(F first, S second) {
		this.first = first;
		this.second = second;
	}

	@Override
	public boolean equals(Object o) {
		Pair p = (Pair)o;
		return p.equals(first) && p.equals(second);
	}

	@Override
	public int hashCode() {
		return (first == null ? 0 : first.hashCode()) ^ (second == null ? 0 : second.hashCode());
	}

	@Override
	public String toString() {
		return "Pair{" + String.valueOf(first) + " " + String.valueOf(second) + "}";
	}
}
