package com.noimagination.srd.ring.powerup;

import com.badlogic.gdx.graphics.Texture;
import com.noimagination.srd.Assets;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.Ring;
import com.noimagination.srd.ring.RingObject;
import com.noimagination.srd.ring.particle.Particle;
import com.noimagination.srd.ring.particle.PowerUpParticle;
import com.noimagination.srd.ring.powerup.effect.Effect;
import com.noimagination.srd.ring.powerup.effect.TempEffect;
import com.noimagination.srd.ring.ship.Player;
import com.noimagination.srd.utils.PolarVector;

public class PowerUp extends RingObject {

	private int rotation = 0;
	private float distance;
	private final Effect effect;

	public PowerUp(Effect effect, int section) {
		super(new PolarVector(Ring.normalizeSection(section) * 45f + 22.5f, 0));
		this.effect = effect;
		getSprite().setColor(effect.getColor());
		setIgnoreColorScheme(true);
	}

	public Effect getEffect () {
		return effect;
	}

	//fixme быдлокод
	@Override
	protected void updateAABB () {
		getAABB().x = getX() - 32;
		getAABB().y = getY() - 32;
		getAABB().width = 64;
		getAABB().height = 64;
	}

	@Override
	public Texture getTexture() {
		return Assets.Textures.POWERUP;	/*effect.getTexture()*/
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		rotation += Ring.SPEED;
		distance += Ring.SPEED;

		moveForward(Ring.SPEED);

		if (distance >= Ring.RADIUS - 25) {
			die();
		}

		collisions();
		getSprite().setRotation(rotation);
	}

	@Override
	public void die () {
		super.die();
		PowerUpParticle.emit(Particle.EmitSize.NORMAL, this);
	}

	private void collisions () {
		Player player = SuperRingDefense.getGame().getPlayer();

		if (player.getAABB().overlaps(getAABB())) {
			die();

			if (effect instanceof TempEffect) {
				TempEffect tempEffect = (TempEffect) effect;
				player.addPowerUp(tempEffect, tempEffect.getDefaultTime());
			} else {
				player.addPowerUp(effect);
			}

			effect.getSound().play();
		}
	}
}
