package com.noimagination.srd.ring.powerup.effect;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.noimagination.srd.Assets;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.color.ColorScheme;
import com.noimagination.srd.ring.Ring;
import com.noimagination.srd.ring.ship.Ship;

public class TimeMachineEffect implements TempEffect {

	private float speedBeforeStop;
	private float timer;
	private float ticks;

	@Override
	public float getDefaultTime() {
		return 3.5f;
	}

	public void resetTicking() {
		timer = 0;
		ticks = 0;
	}

	@Override
	public void update(float delta) {
		timer += delta;

		float rawTime = getDefaultTime() / 5 - ticks * 0.1f;
		float time = rawTime < 0.1f ? 0.1f : rawTime;
		if (timer >= time) {
			timer = 0;
			ticks ++;
			Assets.Sounds.TICK_1.play();
		}
	}

	public void end(Ship ship) {
		Ring.SPEED = speedBeforeStop;
		ship.timemachine = false;
		SuperRingDefense.getGame().setColorScheme(ColorScheme.DEFAULT);
	}

	public void use(Ship ship) {
		speedBeforeStop = Ring.SPEED;
		Ring.SPEED = 0;
		ship.timemachine = true;
		SuperRingDefense.getGame().setColorScheme(ColorScheme.TIMESTOP);
	}

	@Override
	public Sound getSound() {
		return Assets.Sounds.POWERUP_PICK;
	}

	@Override
	public Color getColor() {
		return Assets.Colors.BLUE;
	}

	@Override
	public String getName() {
		return "Time Machine";
	}
}
