package com.noimagination.srd.ring;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.noimagination.srd.Assets;
import com.noimagination.srd.CustomShapeRenderer;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.particle.Particle;
import com.noimagination.srd.ring.powerup.PowerUp;
import com.noimagination.srd.ring.ship.Player;
import com.noimagination.srd.ring.ship.enemy.Enemy;

import java.util.ArrayList;
import java.util.List;

/**
 * Кольцо делится на 8 частей до 45 градусов
 */
public class Ring {
	private static final int DRAW_OFFSET = 20;
	public final static int RADIUS = (SuperRingDefense.WIDTH - DRAW_OFFSET * 2) / 2;
	public final static Vector2 CENTER = new Vector2(SuperRingDefense.WIDTH / 2, SuperRingDefense.HEIGHT / 2);

	private static final float START_SPEED = 5f;	//fixme подумать куда это убрать
	public static float SPEED = START_SPEED;
	private float speedBeforeStop;
	private boolean stopped;
	private Player player;
	private final Map map;

	/* RingObjects */
	public final List<Enemy> enemies = new ArrayList<Enemy>();
	public final List<Bullet> bullets = new ArrayList<Bullet>();
	public final List<Wall> walls = new ArrayList<Wall>();
	public final List<PowerUp> powerUps = new ArrayList<PowerUp>();
	public final List<Particle> particles = new ArrayList<Particle>();

	public Ring () {
		player = new Player();
		map = new Map();
	}

	public static int getRandomSection() {
		return SuperRingDefense.getRandom().nextInt(8);
	}

	public static int normalizeSection(int unsafeSection) {
		int normalizedSector = unsafeSection;
		if (unsafeSection < 0) {
			normalizedSector = 8 + unsafeSection;
		} else if (unsafeSection >= 8) {
			normalizedSector = unsafeSection - 8;
		}
		return normalizedSector;
	}

	/*
	public static boolean isOutOfRing (Vector2 position) {
		return position.dst2(CENTER) > RADIUS * RADIUS;
	}*/

	// fixme временный быдлокод
	public void setPlayer (Player player) {
		this.player = player;
	}

	public Player getPlayer () {
		return player;
	}

	public boolean isStopped () {
		return stopped;
	}

	public void update (float delta) {
		if (stopped) return;

		if (!player.isDead()) player.update(delta);
		if (Ring.SPEED > 0) map.update(delta * Ring.SPEED);

		updateRingObjects(enemies, delta);
		updateRingObjects(bullets, delta);
		updateRingObjects(walls, delta);
		updateRingObjects(powerUps, delta);
		updateRingObjects(particles, delta);
	}

	private void updateRingObjects(List<? extends RingObject> ringObjectList, float delta) {
		for (int i = 0; i < ringObjectList.size(); i++) {
			RingObject ringObject = ringObjectList.get(i);
			if (ringObject == null) continue;

			ringObject.update(delta);

			if (ringObject.isDead()) {
				ringObjectList.remove(ringObject);
				i--;
			}
		}
	}

	private void drawRingObjects (List<? extends RingObject> ringObjectList, SpriteBatch batch, CustomShapeRenderer shapeRenderer) {
		for (RingObject ringObject : ringObjectList) {
			ringObject.draw(batch, shapeRenderer);
		}
	}

	public void draw (SpriteBatch batch, CustomShapeRenderer shapeRenderer) {
		shapeRenderer.setProjectionMatrix(SuperRingDefense.getGame().getCamera().getOrthographicCamera().combined);
		shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
		shapeRenderer.setColor(SuperRingDefense.getGame().getColorScheme().foreground);
		drawRingObjects(walls, batch, shapeRenderer);
		//drawAABB(shapeRenderer);
		shapeRenderer.end();

		batch.setProjectionMatrix(SuperRingDefense.getGame().getCamera().getOrthographicCamera().combined);
		batch.begin();
		batch.setColor(SuperRingDefense.getGame().getColorScheme().foreground);
		batch.draw(Assets.Textures.RING, DRAW_OFFSET, SuperRingDefense.HEIGHT / 2 - RADIUS, RADIUS * 2, RADIUS * 2);
		if (!player.isDead()) player.draw(batch, shapeRenderer);
		drawRingObjects(powerUps, batch, shapeRenderer);
		drawRingObjects(bullets, batch, shapeRenderer);
		drawRingObjects(enemies, batch, shapeRenderer);
		drawRingObjects(particles, batch, shapeRenderer);
		batch.end();
	}

	private void drawAABB (ShapeRenderer shapeRenderer) {
		for (RingObject ringObject : enemies) {
			Rectangle rect = ringObject.getAABB();
			shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
		}

		Rectangle rect = player.getAABB();
		shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);

		for (RingObject ringObject : bullets) {
			Rectangle r = ringObject.getAABB();
			shapeRenderer.rect(r.x, r.y, r.width, r.height);
		}
	}

	public void reset () {
		map.reset();
		Ring.SPEED = Ring.START_SPEED;
		clear();
		resume();
		player.reset();
	}

	public void stop () {
		if (stopped) return;
		stopped = true;
		speedBeforeStop = SPEED;
		SPEED = 0;
	}

	public void resume () {
		if (!stopped) return;
		stopped = false;
		SPEED = speedBeforeStop;
	}

	public void clear () {
		enemies.clear();
		bullets.clear();
		walls.clear();
		powerUps.clear();
		particles.clear();
	}
}
