package com.noimagination.srd.ring;

import com.badlogic.gdx.audio.Sound;
import com.noimagination.srd.ring.ship.Ship;

public interface Usable {
	void use(Ship ship);
	Sound getSound();
}
