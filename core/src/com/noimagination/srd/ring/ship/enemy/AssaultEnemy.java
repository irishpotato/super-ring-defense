package com.noimagination.srd.ring.ship.enemy;

import com.badlogic.gdx.graphics.Texture;
import com.noimagination.srd.Assets;
import com.noimagination.srd.ring.particle.Particle;

public class AssaultEnemy extends Enemy {

	AssaultEnemy(int section) {
		super(section);
	}

	@Override
	public Particle.EmitSize getEmitSize() {
		return Particle.EmitSize.NORMAL;
	}

	@Override
	protected float getReloadTime() {
		return 0.2f;
	}

	@Override
	protected int getMaxHealth() {
		return 1;
	}

	@Override
	public int getDamage() {
		return 1;
	}

	@Override
	public float getBulletSpeed() {
		return 12f;
	}

	@Override
	public int getReward() {
		return 10;
	}

	@Override
	public Texture getTexture() {
		return Assets.Textures.ENEMY;
	}
}
