package com.noimagination.srd.utils;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Utils {
	public static float getAngleBetween(Vector2 a, Vector2 b) {
		float angle = MathUtils.atan2(a.y - b.y, a.x - b.x) * MathUtils.radiansToDegrees;
		return normalizeAngle(angle - 180);
	}

	public static float normalizeAngle (float a) {
		/*
			Абсолютно то же самое, что и:

			if (a < 0)
				a += 360;
			else if (a > 360)
				a -= 360;

			.. но слегка оптимизировано
		 */
		//https://www.xarg.org/2010/06/is-an-angle-between-two-other-angles/
		return (3600000 + a) % 360;	// ????
	}

	public static boolean isAngleBetween (float a, float b, float n) {
		n = (360 + (n % 360)) % 360;	// wtf is this shit
		a = normalizeAngle(a);
		b = normalizeAngle(b);
		if (a < b) return a <= n && n <= b;
		return a <= n || n <= b;
	}

	public static Vector2 getPositionInRing(Vector2 center, float radius, float angle) {
		float rad = MathUtils.degreesToRadians * angle;
		float i = center.x + radius * MathUtils.cos(rad);
		float j = center.y + radius * MathUtils.sin(rad);
		return new Vector2(i, j);
	}
}
