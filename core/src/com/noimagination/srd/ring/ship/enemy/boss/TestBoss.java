package com.noimagination.srd.ring.ship.enemy.boss;

import com.badlogic.gdx.graphics.Texture;
import com.noimagination.srd.ring.particle.Particle;

public class TestBoss extends Boss {

	TestBoss(int section) {
		super(section);
	}

	@Override
	public int getReward() {
		return 0;
	}

	@Override
	public Particle.EmitSize getEmitSize() {
		return null;
	}

	@Override
	protected float getReloadTime() {
		return 0;
	}

	@Override
	protected int getMaxHealth() {
		return 0;
	}

	@Override
	public int getDamage() {
		return 0;
	}

	@Override
	public float getBulletSpeed() {
		return 0;
	}

	@Override
	public Texture getTexture() {
		return null;
	}
}
