package com.noimagination.srd.ring.ship;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.noimagination.srd.Assets;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.Ring;
import com.noimagination.srd.ring.particle.Particle;
import com.noimagination.srd.ring.powerup.effect.EffectType;
import com.noimagination.srd.utils.PolarVector;

public class Player extends Ship {

	public final static float SPEED = 3f;

	public Player () {
		super (new PolarVector(270, Ring.RADIUS));
		getSprite().setColor(Assets.Colors.WHITE);
		setIgnoreColorScheme(true);
	}

	@Override
	public float getReloadTime() {
		return 0.3f;
	}

	@Override
	public int getMaxHealth() {
		return 3;
	}

	@Override
	public int getDamage() {
		return 1;
	}

	@Override
	public float getBulletSpeed() {
		return 24f;
	}

	@Override
	protected void updateSpriteRotation () {
		getSprite().setRotation(getAngle() + 90);
	}

	public void update (float delta) {
		super.update(delta);

		float halfX = Gdx.graphics.getWidth() / 2f;
		float halfY = Gdx.graphics.getHeight() / 2f;
		boolean leftTap = false;
		boolean rightTap = false;
		//fixme БАГ: если одновременно нажать тремя пальцами а потом отпустить, Gdx.input.isTouched не увидит этого
		if (Gdx.app.getType() == Application.ApplicationType.Android || Gdx.app.getType() == Application.ApplicationType.iOS) {
			for (int i = 0; i < 10; i++) { // 20 is max number of touch points (но больше чем 10 маловероятно будет использоваться)
				if (Gdx.input.isTouched(i)) {
					final int x = Gdx.input.getX(i);
					final int y = Gdx.input.getY(i);

					if (y > halfY) {
						if (!leftTap && x < halfX) {	// > halfY, а не <, т.к. 0 по y - это верхний край
							moveClockwise(Player.SPEED);
							leftTap = true;
						}

						if (!rightTap && x > halfX) {
							moveCounterClockwise(Player.SPEED);
							rightTap = true;
						}
					}
				}
			}
		} else {
			if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) moveClockwise(Player.SPEED);
			if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) moveCounterClockwise(Player.SPEED);
		}

		//setPosition(Utils.getPositionInRing(Ring.CENTER, Ring.RADIUS, angle));
		//getSprite().setRotation(Utils.getAngleBetween(getPosition(), Ring.CENTER) - 90);
	}

	@Override
	public Texture getTexture() {
		return Assets.Textures.PLAYER;
	}

	@Override
	public void setAngle (float angle) {
		super.setAngle(angle);
	}

	@Override
	public void shoot () {
		if (SuperRingDefense.getGame().ring.enemies.size() == 0) return;
		super.shoot();
	}

	public void reset() {
		super.reset();
		setAngle(270);
	}

	public void hit (int amount) {
		if (invincible) return;
		super.hit(amount);
		Assets.Sounds.HIT.play();
		SuperRingDefense.getGame().ui.fadeHeartsRed();
		addPowerUp(EffectType.INVINCIBLE.getInstance(), 0.6f);
	}

	@Override
	public void moveForward (float speed) {
		// ничего не делать
	}

	@Override
	public Particle.EmitSize getEmitSize() {
		return Particle.EmitSize.NORMAL;
	}
}
