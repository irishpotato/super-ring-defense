package com.noimagination.srd.ring.ship;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.noimagination.srd.Assets;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.Bullet;
import com.noimagination.srd.ring.Ring;
import com.noimagination.srd.ring.RingObject;
import com.noimagination.srd.ring.particle.Particle;
import com.noimagination.srd.ring.powerup.PowerUpTimer;
import com.noimagination.srd.ring.powerup.effect.Effect;
import com.noimagination.srd.ring.powerup.effect.TempEffect;
import com.noimagination.srd.ring.powerup.effect.TimeMachineEffect;
import com.noimagination.srd.utils.PolarVector;

import java.util.ArrayList;
import java.util.List;

public abstract class Ship extends RingObject {
	private float reloadTimer;
	private int health;
	public boolean invincible;
	public boolean reflect;
	public boolean superammo;
	public boolean timemachine;
	private final List<PowerUpTimer> powerUpsTimer = new ArrayList<PowerUpTimer>();

	public Ship(PolarVector position) {
		super(position);
		health = getMaxHealth();
		setIgnoreColorScheme(true);
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		updateReloadTimer(delta);
		updatePowerUps(delta);
		if (invincible) {
			setInvincibleColor();
		} else {
			getSprite().setAlpha(1f);
		}
	}

	private void updatePowerUps(float delta) {
		for (int i = 0; i < powerUpsTimer.size(); i++) {
			PowerUpTimer timer = powerUpsTimer.get(i);
			if (timer == null) continue;

			timer.update(delta);

			if (timer.ended) {
				powerUpsTimer.remove(i);
				i--;
			}
		}
	}

	public void reset() {
		health = getMaxHealth();
		powerUpsTimer.clear();
		invincible = false;
		superammo = false;
		reflect = false;
	}

	private void updateReloadTimer(float delta) {
		if (!timemachine && Ring.SPEED <= 0) return;
		reloadTimer += delta;

		if (reloadTimer >= (superammo ? 0.1f : getReloadTime())) {
			reloadTimer = 0;
			shoot();
		}
	}

	private void setInvincibleColor() {
		getSprite().getColor().lerp(Assets.Colors.MAGENTA, 1);
		//getSprite().setAlpha(Math.abs(MathUtils.sin(timer * 8)));
	}

	public void hit(int amount) {
		if (invincible) return;
		health -= amount;

		SuperRingDefense.getGame().getCamera().shake(10f);
		if (health <= 0) {
			die();
		}

		fade(Assets.Colors.RED);
	}

	@Override
	public void die () {
		super.die();
		health = 0;
		Assets.Sounds.EXPLOSION.play();
		SuperRingDefense.getGame().getCamera().shake(15f);
	}

	public void restore () {
		health = getMaxHealth();
	}

	public void heal() {
		if (health == getMaxHealth()) return;
		health += 1;
	}

	public void addPowerUp(final Effect effectType, float time) {
		TempEffect tempEffect = (TempEffect)effectType;
		addPowerUp(tempEffect);

		// fixme быдлокод
		PowerUpTimer samePowerUpTimer = null;
		for (PowerUpTimer powerUpTimer : powerUpsTimer) {
			if (powerUpTimer.effect.getClass() == tempEffect.getClass()) {
				samePowerUpTimer = powerUpTimer;
			}
		}

		// Если корабль все еще использует подобный павер ап
		if (samePowerUpTimer != null) {
			samePowerUpTimer.timer = samePowerUpTimer.timer > time ? samePowerUpTimer.timer : time;

			if (samePowerUpTimer.effect instanceof TimeMachineEffect) {
				((TimeMachineEffect) samePowerUpTimer.effect).resetTicking();
			}

			samePowerUpTimer.label.getActions().clear();
			samePowerUpTimer.label.addAction(Actions.sequence(Actions.delay(samePowerUpTimer.timer), Actions.fadeOut(0.5f)));
		} else {
			PowerUpTimer timer = new PowerUpTimer(tempEffect, time, this);
			if (this instanceof Player)
				timer.label = SuperRingDefense.getGame().ui.addPowerUpLabel(tempEffect, time);
			powerUpsTimer.add(timer);
		}
	}

	public void addPowerUp(final Effect effectType) {
		effectType.use(this);
	}

	public void shoot() {
		SuperRingDefense.getGame().ring.bullets.add(new Bullet(this));
		Assets.Sounds.LASER_1.play();
	}

	/*
	@Override
	protected void updateAABB() {
		AABB.x = position.x - 27;
		AABB.y = position.y - 27;
		AABB.width = 54;
		AABB.height = 54;
	}*/

	public int getHealth () {
		return health;
	}

	public abstract Particle.EmitSize getEmitSize();

	protected abstract float getReloadTime();

	protected abstract int getMaxHealth();

	public abstract int getDamage();

	public abstract float getBulletSpeed();
}