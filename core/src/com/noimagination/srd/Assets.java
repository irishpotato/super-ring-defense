package com.noimagination.srd;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

public class Assets {

	public static void init () {
		Fonts.init();
	}

	// Стилистическая цветовая палитра
	public static class Colors {
		public static final Color WHITE 	= getColor(234, 234, 234, 1f);
		public static final Color BLACK 	= getColor(10, 	10, 14, 1f);
		public static final Color RED 		= getColor(237, 54,	54, 1f);
		public static final Color GREEN 	= getColor(0, 225, 0, 1f);
		public static final Color BLUE 		= getColor(85, 170, 255, 1f);
		public static final Color YELLOW 	= getColor(255, 255, 85, 1f);
		public static final Color ORANGE	= getColor(255, 170, 85, 1f);
		public static final Color MAGENTA 	= getColor(226, 102, 255, 1f);
		public static final Color RAINBOW	= getColor (0, 0, 0, 1f);

		private static Color getColor (int r, int g, int b, float a) {
			return new Color(r / 255f, g / 255f, b / 255f, a);
		}
	}

	// Шрифты
	public static class Fonts {
		public static BitmapFont DEFAULT_BIGGER;
		public static BitmapFont DEFAULT;
		public static BitmapFont DEFAULT_SMALLER;

		private static void init () {
			final String FONT_CHARS =
					"абвгдежзийклмнопрстуфхцчшщъыьэюя" +
					"abcdefghijklmnopqrstuvwxyz" +
					"АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" +
					"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
					"0123456789][_!$%#@|\\/?-+=()*&.;:,{}\"´`'<>";

			FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/PressStart2P.ttf"));
			FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.characters = FONT_CHARS;
			parameter.size = 42;
			DEFAULT = generator.generateFont(parameter);
			parameter.size = 32;
			DEFAULT_SMALLER = generator.generateFont(parameter);
			parameter.size = 64;
			DEFAULT_BIGGER = generator.generateFont(parameter);
			generator.dispose();
		}
	}

	// Звуки
	public static class Sounds {
		public static final Sound LASER_1 		= getSound("laser2.ogg");
		public static final Sound HIT 			= getSound("hit.ogg");
		public static final Sound EXPLOSION 	= getSound("explosion1.ogg");
		public static final Sound POWERUP_PICK	= getSound("powerup.ogg");
		public static final Sound TICK_1		= getSound("tick1.ogg");
		public static final Sound TICK_2		= getSound("tick2.ogg");
		public static final Sound TICK_3		= getSound("tick3.ogg");
		public static final Sound CLICK_1		= getSound("click1.ogg");
		public static final Sound CLICK_2		= getSound("click2.ogg");

		private static Sound getSound(String name) {
			return Gdx.audio.newSound(Gdx.files.internal("sounds/" + name));
		}
	}

	// Текстуры
	public static class Textures {
		public static final Texture RING 					= getTexture("ring.png");
		public static final Texture PLAYER 					= getTexture("player.png");
		public static final Texture ENEMY 					= getTexture("enemy.png");
		public static final Texture BIGSHIP					= getTexture("hardEnemy.png");
		public static final Texture BULLET					= getTexture("bullet.png");
		public static final Texture POWERUP 				= getTexture("bonus.png");
		public static final Texture PAUSE_BUTTON			= getTexture("pauseButton.png");
		public static final Texture REPLAY_BUTTON			= getTexture("replayButton.png");
		public static final Texture EXIT_BUTTON				= getTexture("exitButton.png");
		public static final Texture PLAY_BUTTON				= getTexture("playButton.png");
		public static final Texture PAUSE_BUTTON_PRESSED	= getTexture("pauseButtonPressed.png");
		public static final Texture REPLAY_BUTTON_PRESSED	= getTexture("replayButtonPressed.png");
		public static final Texture EXIT_BUTTON_PRESSED		= getTexture("exitButtonPressed.png");
		public static final Texture PLAY_BUTTON_PRESSED		= getTexture("playButtonPressed.png");
		public static final Texture SETTINGS_BUTTON			= getTexture("settingsButton.png");
		public static final Texture SETTINGS_BUTTON_PRESSED	= getTexture("settingsButtonPressed.png");
		public static final Texture FRAGMENT_1				= getTexture("fragment1.png");
		public static final Texture FRAGMENT_2				= getTexture("fragment2.png");
		public static final Texture TITLE					= getTexture("title.png");
		public static final Texture TITLE_LINE				= getTexture("titleLine.png");
		public static final Texture LOGO					= getTexture("logo.png");
		public static final Texture SKULL					= getTexture("skull.png");
		public static final Texture PAUSE_BLACK_PLANE		= getTexture("pauseBlackPlane.png");
		public static final Texture BLACK_LINE 				= getTexture("blackLine.png");
		public static final Texture FULL_HEART				= getTexture("heart.png");
		public static final Texture HALF_HEART				= getTexture("halfHeart.png");


		private static Texture getTexture(String name) {
			return new Texture(Gdx.files.internal("textures/" + name));
		}
	}
}
