package com.noimagination.srd.utils;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import com.noimagination.srd.SuperRingDefense;

public class PolarVector implements Vector<PolarVector> {
	private float angle;
	private float distance;

	public PolarVector () {
		set(0, 0);
	}

	public PolarVector(float angle, float distance) {
		set(angle, distance);
	}

	public PolarVector(PolarVector polarVector) {
		set(polarVector);
	}

	// Не тестировалось
	public static PolarVector cartesianToPolar (Vector2 point, Vector2 center) {
		final float distance = (float)Math.sqrt (point.x * point.x + point.y * point.y);
		final float angle = (float)Math.acos (point.x / distance);
		return new PolarVector (angle, distance);
		//return new PolarVector(Utils.getAngleBetween(point, center), point.dst(center));
	}

	public static Vector2 polarToCartesian (PolarVector polarVector, Vector2 center) {
		final float x = MathUtils.cos(polarVector.getAngle() * MathUtils.degreesToRadians) * polarVector.getDistance() + center.x;
		final float y = MathUtils.sin(polarVector.getAngle() * MathUtils.degreesToRadians) * polarVector.getDistance() + center.y;
		return new Vector2(x, y);
	}

	public float getAngle () {
		return angle;
	}

	public PolarVector setAngle (float angle) {
		this.angle = Utils.normalizeAngle(angle);
		return this;
	}

	public float getDistance () {
		return distance;
	}

	public PolarVector setDistance (float distance) {
		/*
		if (distance < 0) {
			this.distance = -this.distance;
			setAngle(360 - getAngle());
		} else {
			this.distance = distance;
		}*/
		this.distance = distance;
		return this;
	}

	@Override
	public PolarVector cpy() {
		return new PolarVector(angle, distance);
	}

	@Override
	public float len() {
		return getDistance();
	}

	@Override
	public float len2() {
		return len();
	}

	@Override
	public PolarVector limit(float limit) {
		if (getDistance() > limit) setDistance(limit);
		return this;
	}

	@Override
	public PolarVector limit2(float limit2) {
		return limit(limit2);
	}

	@Override
	public PolarVector setLength(float len) {
		return setDistance(len);
	}

	@Override
	public PolarVector setLength2(float len2) {
		return setLength(len2);
	}

	@Override
	public PolarVector clamp(float min, float max) {
		final float dst = getDistance();
		if (dst > max) return setDistance(max);
		if (dst < min) return setDistance(min);
		return this;
	}

	@Override
	public PolarVector set (PolarVector polarVector) {
		set(polarVector.getAngle(), polarVector.getDistance());
		return this;
	}

	public PolarVector set (float angle, float distance) {
		setAngle(angle);
		setDistance(distance);
		return this;
	}

	@Override
	public PolarVector sub(PolarVector v) {
		setAngle(angle - v.angle);
		this.distance = distance - v.distance;
		return this;
	}

	@Override
	public PolarVector nor() {
		return null;
	}

	@Override
	public PolarVector add(PolarVector v) {
		setAngle(angle + v.angle);
		setDistance(distance + v.distance);
		return this;
	}

	public PolarVector add (float angle, float distance) {
		setAngle(getAngle() + angle);
		setDistance(getDistance() + distance);
		return this;
	}

	@Override
	public float dot(PolarVector v) {
		return 0;
	}

	@Override
	public PolarVector scl(float scalar) {
		setAngle(getAngle() * scalar);
		setDistance(getDistance() * scalar);
		return this;
	}

	@Override
	public PolarVector scl(PolarVector v) {
		setAngle(getAngle() * v.getAngle());
		setDistance(getDistance() * v.getDistance());
		return this;
	}

	// Не тестировалось
	@Override
	public float dst(PolarVector v) {
		float deltaDistance = v.getDistance() - getDistance();
		float deltaAngle = Utils.normalizeAngle(getAngle() - v.getAngle());
		return (float)Math.sqrt(deltaDistance * deltaDistance + getDistance() * getDistance() * (deltaAngle * deltaAngle));
	}

	@Override
	public float dst2(PolarVector v) {
		float deltaDistance = v.getDistance() - getDistance();
		float deltaAngle = Utils.normalizeAngle(getAngle() - v.getAngle());

		return deltaDistance * deltaDistance + getDistance() * getDistance() * (deltaAngle * deltaAngle);
	}

	@Override
	public PolarVector lerp (PolarVector target, float alpha) {
		final float invAlpha = 1.0f - alpha;
		setAngle((getAngle() * invAlpha) + (angle * alpha));
		setDistance((getDistance() * invAlpha) + (distance * alpha));
		return this;
	}

	public PolarVector lerpDistance (float distance, float alpha) {
		final float invAlpha = 1.0f - alpha;
		setDistance((getDistance() * invAlpha) + (distance * alpha));
		return this;
	}

	public PolarVector lerpAngle (float angle, float alpha) {
		setAngle(MathUtils.lerpAngleDeg(getAngle(), angle, alpha));
		return this;
	}

	@Override
	public PolarVector interpolate(PolarVector target, float alpha, Interpolation interpolator) {
		return lerp(target, interpolator.apply(alpha));
	}

	@Override
	public PolarVector setToRandomDirection() {
		float angle = SuperRingDefense.getRandom().nextInt(360);
		float distance = SuperRingDefense.getRandom().nextFloat();
		set(angle, distance);
		return this;
	}

	@Override
	public boolean isUnit() {
		return false;
	}

	@Override
	public boolean isUnit(float margin) {
		return false;
	}

	@Override
	public boolean isZero() {
		return false;
	}

	@Override
	public boolean isZero(float margin) {
		return false;
	}

	@Override
	public boolean isOnLine(PolarVector other, float epsilon) {
		return false;
	}

	@Override
	public boolean isOnLine(PolarVector other) {
		return false;
	}

	@Override
	public boolean isCollinear(PolarVector other, float epsilon) {
		return false;
	}

	@Override
	public boolean isCollinear(PolarVector other) {
		return false;
	}

	@Override
	public boolean isCollinearOpposite(PolarVector other, float epsilon) {
		return false;
	}

	@Override
	public boolean isCollinearOpposite(PolarVector other) {
		return false;
	}

	@Override
	public boolean isPerpendicular(PolarVector other) {
		return false;
	}

	@Override
	public boolean isPerpendicular(PolarVector other, float epsilon) {
		return false;
	}

	@Override
	public boolean hasSameDirection(PolarVector other) {
		return false;
	}

	@Override
	public boolean hasOppositeDirection(PolarVector other) {
		return false;
	}

	@Override
	public boolean epsilonEquals(PolarVector other, float epsilon) {
		return false;
	}

	@Override
	public PolarVector mulAdd(PolarVector v, float scalar) {
		return null;
	}

	@Override
	public PolarVector mulAdd(PolarVector v, PolarVector mulVec) {
		return null;
	}

	@Override
	public PolarVector setZero() {
		return null;
	}
}
