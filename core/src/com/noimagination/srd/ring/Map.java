package com.noimagination.srd.ring;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.powerup.PowerUp;
import com.noimagination.srd.ring.powerup.PowerUpTimer;
import com.noimagination.srd.ring.powerup.effect.EffectType;
import com.noimagination.srd.ring.ship.Player;
import com.noimagination.srd.ring.ship.enemy.Enemy;
import com.noimagination.srd.ring.ship.enemy.EnemyType;

import java.util.ArrayList;
import java.util.List;

public class Map {

	private static final List<MapAction> queue = new ArrayList<MapAction>();
	private static float delayTime;
	private MapType currentMapType;
	private int leadSection;
	private static boolean delayUntilEnd;

	public Map() {
		if (MapType.values().length <= 1) {
			Gdx.app.error("SuperRingDefense Map", "MapType.values().length is <= 1.");
		} else {
			reset();
		}
	}

	public void reset () {
		queue.clear();
		delayTime = 0;
		leadSection = Ring.getRandomSection();
		nextMap();
	}

	private static void delayUntilEnd () {
		queue.add(new MapAction() {
			@Override
			public void action() {
				delayUntilEnd = true;
			}
		});
	}

	private static void delay(final float time) {
		queue.add(new MapAction() {
			@Override
			public void action() {
				delayTime = time;
			}
		});
	}

	/*
	private static void scaleWall (final Wall wall, final float angleA, final float angleB) {
		queue.add(new MapAction() {
			@Override
			public void action() {
				wall.scale(angleA, angleB);
			}
		});
	}

	private static void scale (final Wall wall, final float angleA, final float angleB) {
		queue.add(new MapAction() {
			@Override
			public void action() {
				wall.scale(angleA, angleB);
			}
		});
	}

	private static void scaleWallClockwise (final Wall wall, final float angle) {
		queue.add(new MapAction() {
			@Override
			public void action() {
				wall.scaleClockwise(angle);
			}
		});
	}

	private static void scaleWallCounterClockwise (final Wall wall, final float angle) {
		queue.add(new MapAction() {
			@Override
			public void action() {
				wall.scaleCounterClockwise(angle);
			}
		});
	}

	private static void moveWallClockwise (final Wall wall, final float angle) {
		queue.add(new MapAction() {
			@Override
			public void action() {
				wall.moveClockwise(angle);
			}
		});
	}

	private static void moveWallCounterClockwise (final Wall wall, final float angle) {
		queue.add(new MapAction() {
			@Override
			public void action() {
				wall.moveCounterClockwise(angle);
			}
		});
	}*/

	private static Wall wall(final int width, final int... section) {
		final Wall wall = new Wall(width, section);
		queue.add(new MapAction() {
			@Override
			public void action() {
				SuperRingDefense.getGame().ring.walls.add(wall);
			}
		});

		return wall;
	}

	private static Enemy enemy(final EnemyType enemyType, final int section) {
		final Enemy enemy = enemyType.getInstance(section);
		queue.add(new MapAction() {
			@Override
			public void action() {
				SuperRingDefense.getGame().ring.enemies.add(enemy);
			}
		});

		return enemy;
	}

	private static PowerUp powerUp(final EffectType effectType, final int section) {
		final PowerUp powerUp = new PowerUp(effectType.getInstance(), section);
		queue.add(new MapAction() {
			@Override
			public void action() {
				SuperRingDefense.getGame().ring.powerUps.add(powerUp);
			}
		});

		return powerUp;
	}

	// fixme помоему быдлокод
	// fixme не работает delay() в начале и в конце MapType
	public void update(float delta) {
		if (delayTime > 0) {
			delayTime -= delta;

			if (delayTime < 0) {
				delayTime = 0;
			}
		}

		if (delayTime == 0) {
			if (delayUntilEnd) {
				if (SuperRingDefense.getGame().ring.enemies.isEmpty() && SuperRingDefense.getGame().ring.walls.isEmpty()) {
					delayUntilEnd = false;
				}
			} else {
				if (queue.size() > 0) {
					moveQueue();
				} else {
					nextMap();
				}
			}
		}
	}

	private void moveQueue () {
		MapAction nextAction = queue.get(0);
		nextAction.action();
		queue.remove(nextAction);
	}

	private void useMap(MapType type) {
		currentMapType = type;
		type.mapGenerator.generate(leadSection);
		delayUntilEnd();
	}

	private MapType getRandomMap() {
		MapType mapType = MapType.values()[SuperRingDefense.getRandom().nextInt(MapType.values().length)];
		//MapType mapType = level.mapTypes[SuperRingDefense.random.nextInt(level.mapTypes.length)];

		if (currentMapType != null && currentMapType.equals(mapType)) {
			return getRandomMap();
		}

		return mapType;
	}

	private void nextMap() {
		moveLeadSection();
		useMap(getRandomMap());
	}

	private void moveLeadSection() {
		int random = SuperRingDefense.getRandom().nextInt(5) - 4;
		leadSection = Ring.normalizeSection(leadSection + random);
	}

	enum MapType {

		WINDMILL (new MapGenerator() {
			@Override
			public void generate(int leadSection) {
				/*
				wall(300, 0).addAction(RingObject.Actions.alwaysMoveClockwise(Player.SPEED));
				wall(300, 2).addAction(RingObject.Actions.alwaysMoveClockwise(Player.SPEED));
				wall(300, 4).addAction(RingObject.Actions.alwaysMoveClockwise(Player.SPEED));
				wall(300, 6).addAction(RingObject.Actions.alwaysMoveClockwise(Player.SPEED));
				delay(6.5f);
				wall(300, 1).addAction(RingObject.Actions.alwaysMoveCounterClockwise(Player.SPEED));
				wall(300, 3).addAction(RingObject.Actions.alwaysMoveCounterClockwise(Player.SPEED));
				wall(300, 5).addAction(RingObject.Actions.alwaysMoveCounterClockwise(Player.SPEED));
				wall(300, 7).addAction(RingObject.Actions.alwaysMoveCounterClockwise(Player.SPEED));
				delay(7f);*/
				powerUp(EffectType.INVINCIBLE, 2);
			}
		}),

		WINDMILL2 (new MapGenerator() {
			@Override
			public void generate(int leadSection) {
				Wall w1 = wall(300, leadSection);
				if (SuperRingDefense.getRandom().nextBoolean()) {
					w1.addAction(RingObject.Actions.alwaysMoveClockwise(Player.SPEED));
				} else {
					w1.addAction(RingObject.Actions.alwaysMoveCounterClockwise(Player.SPEED));
				}

				w1.addAction(Wall.Actions.scale(260, 260), 300);
			}
		}),

		ENEMY_WAVE(new MapGenerator() {
			@Override
			public void generate(int leadSection) {
				Wall w1 = wall(500, 0);
				w1.addAction(RingObject.Actions.alwaysMoveCounterClockwise(1));
				w1.addAction(RingObject.Actions.alwaysMoveClockwise(1), 400);
				w1.addAction(Wall.Actions.scale(90, 90), 400);
				delayUntilEnd();
				enemy(EnemyType.ASSAULT, 2);
			}
		}),
		;

		final MapGenerator mapGenerator;

		MapType(MapGenerator mapGenerator) {
			this.mapGenerator = mapGenerator;
		}
	}

	interface MapAction {
		void action();
	}

	interface MapGenerator {
		void generate(int leadSection);
	}
}
