package com.noimagination.srd.ring;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.noimagination.srd.Assets;
import com.noimagination.srd.CustomShapeRenderer;
import com.noimagination.srd.GameObject;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.utils.PolarVector;

import java.util.HashMap;
import java.util.Iterator;

public abstract class RingObject implements GameObject {
	private boolean dead;
	private final Sprite sprite;
	private final PolarVector position;
	protected float timer;
	protected float fadeTimer;
	private boolean ignoreColorScheme;
	private Color fadeColor;
	private Color colorBeforeFade;

	public static final float FADE_TIME = 0.2f;

	float circleMoveSpeed;
	private final HashMap<Float, Wall.Action> actions = new HashMap<Float, Wall.Action>();

	@Deprecated
	private final Rectangle AABB = new Rectangle();

	public RingObject(PolarVector position) {
		// fixme нормально handle'ить это если texture == null
		Texture texture = getTexture();
		if (texture != null) {
			this.sprite = new Sprite(texture);
		} else {
			this.sprite = new Sprite(Assets.Textures.LOGO);	// botteneck
		}

		this.position = position;
		updateSprite();
	}

	public void fade (Color color) {
		if (fadeTimer > 0f) return;
		this.fadeColor = color;
		colorBeforeFade = getSprite().getColor();
		fadeTimer = FADE_TIME;
	}

	@Deprecated
	public Rectangle getAABB() {
		return AABB;
	}

	@Deprecated
	protected void updateAABB () {
		final float max = Math.max(getSprite().getWidth(), getSprite().getHeight());
		Vector2 cartesian = getCartesianPosition();
		AABB.x = cartesian.x - max / 2;
		AABB.y = cartesian.y - max / 2;
		AABB.width = max;
		AABB.height = max;
	}

	public abstract Texture getTexture ();

	public Sprite getSprite () {
		return sprite;
	}

	public final void setPosition (PolarVector position) {
		setPosition(position.getAngle(), position.getDistance());
	}

	public final void setPosition (float angle, float distance) {
		setAngle(angle);
		setDistance(distance);
	}

	interface Action {
		void action (RingObject target);
	}

	public void addAction(Action action, float distance) {
		actions.put(distance, action);
	}

	public void addAction(Action action) {
		addAction(action, 0);
	}

	private void updateActions() {
		// То что ниже - это foreach, но не кидающий ConcurrentModificationException
		Iterator<java.util.Map.Entry<Float, Action>> it = actions.entrySet().iterator();

		while (it.hasNext())
		{
			java.util.Map.Entry<Float, Action> item = it.next();
			if (getDistance() >= item.getKey()) {
				item.getValue().action(this);
				it.remove();
			}
		}
	}

	public PolarVector getPosition () {
		return position;
	}

	public Vector2 getCartesianPosition () {
		return PolarVector.polarToCartesian(position, Ring.CENTER);
	}

	public float getX () {
		return PolarVector.polarToCartesian(position, Ring.CENTER).x;
	}

	public float getY () {
		return PolarVector.polarToCartesian(position, Ring.CENTER).y;
	}

	public void setAngle (float angle) {
		position.setAngle(angle);
		updateSprite();
	}

	public float getAngle () {
		return position.getAngle();
	}

	public void setDistance (float distance) {
		position.setDistance(distance);
		updateSprite();
	}

	protected void updateSprite () {
		updateSpritePosition();
		updateSpriteRotation();
	}

	protected void updateSpritePosition () {
		Vector2 cartesian = PolarVector.polarToCartesian(getPosition(), Ring.CENTER);
		getSprite().setCenter(cartesian.x, cartesian.y);
	}

	protected void updateSpriteRotation () {
		getSprite().setRotation(getAngle() - 90);
	}

	public float getDistance () {
		return position.getDistance();
	}

	public void die () {
		dead = true;
	}

	public boolean isDead () {
		return dead;
	}

	protected void moveForward (float speed) {
		setDistance(getDistance() + speed);
	}

	protected void moveClockwise(float angle) {
		setAngle(getAngle() + angle);
	}

	protected void moveCounterClockwise(float angle) {
		setAngle(getAngle() - angle);
	}

	private void alwaysMoveClockwise(float speed) {
		circleMoveSpeed = speed;
	}

	private void alwaysMoveCounterClockwise (float speed) {
		circleMoveSpeed = -speed;
	}

	static class Actions {
		public static Action moveClockwise (final float angle) {
			return new Action() {
				@Override
				public void action(RingObject target) {
					target.moveClockwise(angle);
				}
			};
		}

		public static Action moveCounterClockwise (final float angle) {
			return new Action() {
				@Override
				public void action(RingObject target) {
					target.moveCounterClockwise(angle);
				}
			};
		}

		public static Action alwaysMoveClockwise (final float speed) {
			return new Action() {
				@Override
				public void action(RingObject target) {
					target.alwaysMoveClockwise(speed);
				}
			};
		}

		public static Action alwaysMoveCounterClockwise (final float speed) {
			return new Action() {
				@Override
				public void action(RingObject target) {
					target.alwaysMoveCounterClockwise(speed);
				}
			};
		}
	}

	protected void updateFade (float delta) {
		if (fadeTimer > 0f) {
			fadeTimer -= delta;
			getSprite().setColor(getSprite().getColor().lerp(fadeColor, FADE_TIME - fadeTimer));

			if (fadeTimer < 0f) {
				System.out.println("oh");
				fadeTimer = 0;
				getSprite().setColor(SuperRingDefense.getGame().getColorScheme().foreground);
			}
		}
	}

	@Override
	public void update (float delta) {
		timer += Ring.SPEED / 100;

		position.setAngle(getPosition().getAngle() + circleMoveSpeed);
		updateAABB();
		updateActions();
		updateFade(delta);
	}

	public void setIgnoreColorScheme (boolean ignoreColorScheme) {
		this.ignoreColorScheme = ignoreColorScheme;
	}

	public boolean getIgnoreColorScheme () {
		return ignoreColorScheme;
	}

	@Override
	public void draw (SpriteBatch batch, CustomShapeRenderer shapeRenderer) {
		if (!getIgnoreColorScheme()) getSprite().setColor(SuperRingDefense.getGame().getColorScheme().foreground);
		getSprite().draw(batch);
	}
}