package com.noimagination.srd.ring.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.noimagination.srd.Assets;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.ring.powerup.PowerUp;

public class PowerUpParticle extends Particle {

	private final PowerUp powerUp;

	private PowerUpParticle(PowerUp powerUp) {
		super(powerUp.getPosition());
		this.powerUp = powerUp;
		updateColor();
	}

	public static void emit(EmitSize size, PowerUp powerUp) {
		for (int i = 0; i < size.ordinal() * 3 + SuperRingDefense.getRandom().nextInt(size.ordinal() * 3 / 2); i++) {
			SuperRingDefense.getGame().ring.particles.add(new PowerUpParticle(powerUp));
		}
	}

	@Override
	public Texture[] getTextureSet() {
		return new Texture[] {
				Assets.Textures.FRAGMENT_1
		};
	}

	@Override
	public Color getColor() {
		return powerUp.getEffect().getColor();
	}
}
