package com.noimagination.srd.ring;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.noimagination.srd.CustomShapeRenderer;
import com.noimagination.srd.SuperRingDefense;
import com.noimagination.srd.color.ColorScheme;
import com.noimagination.srd.ring.particle.ShipFragmentParticle;
import com.noimagination.srd.ring.ship.Player;
import com.noimagination.srd.utils.PolarVector;
import com.noimagination.srd.utils.Utils;

public class Wall extends RingObject {

	private final PolarVector pointA;
	private final PolarVector pointB;
	private final PolarVector pointC;
	private final PolarVector pointD;

	/* Lerp variables */
	private float angleAtarget;
	private float angleBtarget;

	private Interpolation scaleInterpolation;
	private Color color;

	public Wall (int width, int... section) {
		this (width, section[0] * 45f, section[section.length - 1] * 45f + 45f);
	}

	public Wall (int width, float angleA, float angleB) {
		super(new PolarVector((Utils.normalizeAngle(angleA) + Utils.normalizeAngle(angleB)) / 2, 0))/*, width*/;

		pointA = new PolarVector(angleA, -width);
		pointB = new PolarVector(angleB, -width);
		pointC = new PolarVector(angleA, 0);
		pointD = new PolarVector(angleB, 0);

		angleAtarget = angleA;
		angleBtarget = angleB;

		scaleInterpolation = Interpolation.smooth;
	}

	protected void scaleClockwise (float angle, Interpolation interpolation) {
		this.angleAtarget = pointA.getAngle() - angle;
		scaleInterpolation = interpolation;
	}

	protected void scaleCounterClockwise(float angle, Interpolation interpolation) {
		this.angleBtarget = pointB.getAngle() + angle;
		scaleInterpolation = interpolation;
	}

	protected void scale(float angleA, float angleB, Interpolation interpolation) {
		scaleClockwise(angleA, interpolation);
		scaleCounterClockwise(angleB, interpolation);
	}

	static class Actions extends RingObject.Actions {

		public static Action scaleClockwise (final float angle) {
			return scaleClockwise(angle, Interpolation.smooth);
		}

		public static Action scaleCounterClockwise (final float angle) {
			return scaleCounterClockwise(angle, Interpolation.smooth);
		}

		public static Action scale (final float angleA, final float angleB) {
			return scale(angleA, angleB, Interpolation.smooth);
		}

		public static Action scaleClockwise (final float angle, final Interpolation interpolation) {
			return new Action() {
				@Override
				public void action(RingObject target) {
					((Wall)target).scaleClockwise(angle, interpolation);
				}
			};
		}

		public static Action scaleCounterClockwise (final float angle, final Interpolation interpolation) {
			return new Action() {
				@Override
				public void action(RingObject target) {
					((Wall)target).scaleCounterClockwise(angle, interpolation);
				}
			};
		}

		public static Action scale (final float angleA, final float angleB, final Interpolation interpolation) {
			return new Action() {
				@Override
				public void action(RingObject target) {
					((Wall)target).scale(angleA, angleB, interpolation);
				}
			};
		}
	}

	@Override
	public Texture getTexture() {
		return null;
	}

	@Override
	public void moveForward (float speed) {
		super.moveForward(speed);
		pointA.add(circleMoveSpeed, speed);
		pointB.add(circleMoveSpeed, speed);
		pointD.add(circleMoveSpeed, speed);
		pointC.add(circleMoveSpeed, speed);

		angleAtarget += circleMoveSpeed;
		angleBtarget += circleMoveSpeed;

		if (pointA.getDistance() >= Ring.RADIUS) {
			die();
		}
	}

	private void lerpAngles (float speed) {
		if (Utils.normalizeAngle(angleAtarget) != pointA.getAngle() ||
				Utils.normalizeAngle(angleBtarget) != pointB.getAngle()) {
			timer += speed;

			float angleAtargetNormalized = Utils.normalizeAngle(angleAtarget);
			if (angleAtargetNormalized != pointA.getAngle()) {
				pointA.lerpAngle(angleAtargetNormalized, scaleInterpolation.apply(timer));
				pointC.lerpAngle(angleAtargetNormalized, scaleInterpolation.apply(timer));

				if (timer >= 1f) {
					pointB.setAngle(angleAtargetNormalized);
				}
			}

			float angleBtargetNormalized = Utils.normalizeAngle(angleBtarget);
			if (angleBtargetNormalized != pointB.getAngle()) {
				pointB.lerpAngle(angleBtargetNormalized, scaleInterpolation.apply(timer));
				pointD.lerpAngle(angleBtargetNormalized, scaleInterpolation.apply(timer));

				if (timer >= 1f) {
					pointB.setAngle(angleBtargetNormalized);
				}
			}
		} else {
			timer = 0;
		}
	}

	@Override
	public void moveClockwise(float speed) {
		this.angleAtarget = this.pointA.getAngle() + speed;
		this.angleBtarget = this.pointB.getAngle() + speed;
	}

	@Override
	public void moveCounterClockwise(float speed) {
		this.angleAtarget = this.pointA.getAngle() - speed;
		this.angleBtarget = this.pointB.getAngle() - speed;
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		collisions();
		if (Ring.SPEED <= 0) return;
		moveForward(Ring.SPEED);
		lerpAngles(Ring.SPEED / 800f);
	}

	@Override
	public void draw(SpriteBatch batch, CustomShapeRenderer shapeRenderer) {
		// "Обрезаем" точки clamp'ом для того, что бы линии и арки рисовались корректно
		if (fadeTimer <= 0) {
			color = SuperRingDefense.getGame().getColorScheme().foreground.cpy();
		}

		shapeRenderer.setColor(color);
		final PolarVector clampedPointA = pointA.cpy().clamp(0, Ring.RADIUS);
		final PolarVector clampedPointB = pointB.cpy().clamp(0, Ring.RADIUS);
		final PolarVector clampedPointC = pointC.cpy().clamp(0, Ring.RADIUS);
		final PolarVector clampedPointD = pointD.cpy().clamp(0, Ring.RADIUS);

		float angle = Utils.normalizeAngle(pointB.getAngle()-pointA.getAngle());
		shapeRenderer.arc(Ring.CENTER.x, Ring.CENTER.y, clampedPointA.getDistance(), clampedPointA.getAngle(), angle);
		shapeRenderer.arc(Ring.CENTER.x, Ring.CENTER.y, clampedPointC.getDistance() < 0f ? 0 : clampedPointC.getDistance(), clampedPointA.getAngle(), angle);

		final Vector2 cartesianA = PolarVector.polarToCartesian(clampedPointA, Ring.CENTER);
		final Vector2 cartesianB = PolarVector.polarToCartesian(clampedPointB, Ring.CENTER);
		final Vector2 cartesianC = PolarVector.polarToCartesian(clampedPointC, Ring.CENTER);
		final Vector2 cartesianD = PolarVector.polarToCartesian(clampedPointD, Ring.CENTER);
		shapeRenderer.line(cartesianA.x, cartesianA.y, cartesianC.x, cartesianC.y);
		shapeRenderer.line(cartesianB.x, cartesianB.y, cartesianD.x, cartesianD.y);
	}

	@Override
	protected void updateFade (float delta) {
		if (fadeTimer > 0f) {
			fadeTimer -= delta;
			color.lerp(SuperRingDefense.getGame().getColorScheme().background, 1f - fadeTimer);
		}
	}

	private void collisions () {
		Player player = SuperRingDefense.getGame().getPlayer();
		float offset = 3.5f;

		if (Utils.isAngleBetween(pointA.getAngle() - offset, pointB.getAngle() + offset, player.getAngle())) {
			if (pointC.getDistance() >= Ring.RADIUS - 20) {
				if (!player.isDead() && !player.invincible) {
					ShipFragmentParticle.emit(player.getEmitSize(), player);
					player.hit(1);
					//fade();
				}
			}
		}
	}
}
