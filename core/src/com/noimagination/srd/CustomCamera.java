package com.noimagination.srd;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.noimagination.srd.ring.Ring;

public class CustomCamera {

	private final OrthographicCamera camera;
	private boolean isShaking;
	private float shakeRadius;
	private float shakeRandomAngle;
	private Vector2 shakeAngle;
	private float targetZoom;
	private float lerpTimer;
	private int shakes;

	public CustomCamera(OrthographicCamera camera) {
		this.camera = camera;
		targetZoom = camera.zoom;
		camera.translate(SuperRingDefense.WIDTH / 2, SuperRingDefense.HEIGHT / 2);
		camera.update();
	}

	public void update (float delta) {
		if (isShaking) {
			updateShake();
		}

		if (camera.zoom != targetZoom) {
			lerpTimer += delta;

			camera.zoom = MathUtils.lerp(camera.zoom, targetZoom, Interpolation.pow2.apply(lerpTimer));

			if (lerpTimer >= 1) {
				camera.zoom = targetZoom;
			}
		}
	}

	public OrthographicCamera getOrthographicCamera () {
		return camera;
	}

	public void smoothZoom (float targetZoom) {
		this.targetZoom = targetZoom;
	}

	private void updateShake() {
		if (shakes >= 2) {
			camera.position.set(Ring.CENTER, 0);
			shakes = 0;
		}

		shakeRadius *= 0.9f;
		shakeRandomAngle += (180 + SuperRingDefense.getRandom().nextInt(60));
		shakeAngle.set(MathUtils.sin(shakeRandomAngle) * shakeRadius, MathUtils.cos(shakeRandomAngle) * shakeRadius);
		camera.translate(shakeAngle);

		if (shakeRadius < 2f) {
			isShaking = false;
		}

		shakes++;

		camera.update();
	}

	public void shake(float radius) {
		this.shakeRadius = radius;
		shakeRandomAngle = SuperRingDefense.getRandom().nextInt(360);
		shakeAngle = new Vector2(MathUtils.sin(shakeRandomAngle) * radius, MathUtils.cos(shakeRandomAngle) * radius);
		camera.translate(shakeAngle);
		isShaking = true;
	}
}
