package com.noimagination.srd;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface GameObject {
	void draw (SpriteBatch batch, CustomShapeRenderer shapeRenderer);
	void update (float delta);
}
